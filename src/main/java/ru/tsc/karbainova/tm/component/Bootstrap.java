package ru.tsc.karbainova.tm.component;

import ru.tsc.karbainova.tm.api.repository.*;
import ru.tsc.karbainova.tm.api.service.*;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.auth.*;
import ru.tsc.karbainova.tm.command.project.*;
import ru.tsc.karbainova.tm.command.serv.*;
import ru.tsc.karbainova.tm.command.task.*;
import ru.tsc.karbainova.tm.constant.TerminalConst;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.system.UnknowCommandException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.repository.*;
import ru.tsc.karbainova.tm.service.*;

import java.util.Scanner;

public class Bootstrap implements ServiceLocator {

    private final IUserRepository userRepository = new UserRepository();
    private final IUserService userService = new UserService(userRepository);

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectToTaskService projectToTaskService = new ProjectToTaskService(projectRepository, taskRepository);

    private final ILogService logService = new LogService();

    private final IAuthService authService = new AuthService(userService);

    public void start(String[] args) {
        displayWelcome();
        initDate();
        runArgs(args);
        logService.debug("Test environment");
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            try {
                System.out.println("Enter command:");
                command = scanner.nextLine();
                logService.command(command);
                ranCommands(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }


    private void initDate() {
        userService.create("test", "test", "test");
        userService.create("admin", "admin", Role.ADMIN);
    }

    {
        registry(new AuthChangePasswordCommand());
        registry(new AuthLoginCommand());
        registry(new AuthLogoutCommand());
        registry(new AuthRegistryCommand());
        registry(new AuthUpdateProfileCommand());
        registry(new AuthViewProfileCommand());

        registry(new ShowCommandsCommand());
        registry(new ShowArgumentsCommand());
        registry(new AboutDisplayCommand());
        registry(new ExitCommand());
        registry(new HelpDisplayCommand());
        registry(new InfoDisplayCommand());
        registry(new VersionDisplayCommand());

        registry(new ProjectListShowCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskListShowCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskBindTaskToProjectByIdCommand());
        registry(new TaskUnbindByIdCommand());
        registry(new TaskFindAllTaskByProjectIdCommand());

    }

    private boolean runArgs(final String[] args) {
        if (args == null) return false;
        if (args.length < 1) return false;
        AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknowCommandException(args[0]);
        command.execute();
        return true;
    }

    private void ranCommands(final String command) {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknowCommandException(command);
        abstractCommand.execute();
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.create(command);
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectToTaskService getProjectToTaskService() {
        return projectToTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }
}
