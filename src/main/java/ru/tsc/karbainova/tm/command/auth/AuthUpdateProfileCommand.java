package ru.tsc.karbainova.tm.command.auth;

import ru.tsc.karbainova.tm.command.AuthAbstractCommand;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class AuthUpdateProfileCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "update-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update profile";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter First name");
        final String first = TerminalUtil.nextLine();
        System.out.println("Enter Last name");
        final String last = TerminalUtil.nextLine();
        System.out.println("Enter Middle name");
        final String middle = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(userId, first, last, middle);
    }
}
