package ru.tsc.karbainova.tm.enumerated;

import ru.tsc.karbainova.tm.comparator.ComparatorByCreatedDate;
import ru.tsc.karbainova.tm.comparator.ComparatorByName;
import ru.tsc.karbainova.tm.comparator.ComparatorByStartDate;
import ru.tsc.karbainova.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {
    NAME("sort by name", ComparatorByName.getInstance()),
    CREATED("sort by created", ComparatorByCreatedDate.getInstance()),
    STATUS("sort by status", ComparatorByStatus.getInstance()),
    START_DATE("sort by start date", ComparatorByStartDate.getInstance());

    private final String displayName;
    private final Comparator comparator;

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
