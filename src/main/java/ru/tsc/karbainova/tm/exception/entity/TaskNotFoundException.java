package ru.tsc.karbainova.tm.exception.entity;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {
    public TaskNotFoundException() {
        super("Error Task not found.");
    }
}
