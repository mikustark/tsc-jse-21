package ru.tsc.karbainova.tm.model;

import ru.tsc.karbainova.tm.api.entity.IWBS;

public abstract class AbstractOwnerEntity extends AbstractEntity implements IWBS {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
