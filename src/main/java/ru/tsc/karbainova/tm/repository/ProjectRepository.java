package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.model.Project;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {
    private final List<Project> projects = new ArrayList<>();

    @Override
    public boolean existsById(String userId, String id) {
        return findById(userId, id) != null;
    }

    @Override
    public Project findByIndex(String userId, int index) {
        final List<Project> entities = findAll(userId);
        return entities.get(index);
    }

    @Override
    public Project findByName(String userId, String name) {
        for (Project project : entities) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeByName(String userId, String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(String userId, int index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }
}
